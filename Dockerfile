FROM registry.gitlab.com/froscon/php-track-web/alpine-node-builder:1.1.0 as node_builder

ENV NPM_CONFIG_CACHE /cache/npm

COPY /app/src/.babelrc /app/src/package.json /app/src/webpack.config.js /app/src/
WORKDIR /app/src/
RUN yarn install
COPY /app/src/resources/assets/ /app/src/resources/assets
RUN yarn build

FROM registry.gitlab.com/froscon/php-track-web/alpine-php7.2-builder:1.1.0 as composer_builder

ARG composer_cache_dir="/build_cache/composer/"
ENV COMPOSER_HOME $composer_cache_dir
WORKDIR /app/src/
COPY / /

RUN apk update \
    && apk add --no-cache \
            libpng \
            libpng-dev \
            gnupg \
            openssl \
            git \
            curl \
            mysql-client \
            icu-dev \
            gettext-dev && \
    apk add --update  php7-gd \ 
            php7-gettext \
            # php7-exif \
            php7-dom \
            php7-pdo_mysql \
            php7-pdo_sqlite \
            php7-bz2 \
            php7-opcache \
            php7-tokenizer && \
    /usr/local/bin/composer-install-wrapper.sh

# Build the PHP container
FROM registry.gitlab.com/froscon/php-track-web/alpine-php-fpm7.2-nginx:1.1.0 as server

# RUN find /app/src/storage/ -type f -not -name .gitignore -exec rm {} \;  && rm -f /app/src/import/* /app/src/config/config.php

WORKDIR /app/src/

RUN apk update \
    && apk add --no-cache \
            libpng \
            libpng-dev \
            gnupg \
            openssl \
            git \
            curl \
            mysql-client \
            icu-dev \
            gettext-dev && \
    apk add --update  php7-gd \ 
            php7-gettext \
            # php7-exif \
            php7-dom \
            php7-pdo_mysql \
            php7-pdo_sqlite \
            php7-bz2 \
            php7-opcache \
            php7-tokenizer

ENV TRUSTED_PROXIES 10.0.0.0/8,::ffff:10.0.0.0/8,\
                    127.0.0.0/8,::ffff:127.0.0.0/8,\
                    172.16.0.0/12,::ffff:172.16.0.0/12,\
                    192.168.0.0/16,::ffff:192.168.0.0/16,\
                    ::1/128,fc00::/7,fec0::/10


ENV VERSION_TAG 0.3.17
LABEL image.name=engelsystem \
      image.version=0.3.17 \
      image.tag=registry.gitlab.com/developersforfuture/registry/engelsystem/app-production \
      image.scm.commit=$commit \
      image.scm.url=git@github.com:developersforfuture/engelsystem.git \
      image.author="Maximilian Berghoff <maximilian.berghoff@gmx.de>"


FROM registry.gitlab.com/wir-vs-climatecrisis/engelsystem/server:latest as production

COPY / /
COPY --from=node_builder /app/src/public/assets/ /app/src/public/assets
COPY --from=composer_builder /app/src/vendor/ /app/src/vendor/


RUN  cp /app/src/config/config.default.php /app/src/config/config.php

WORKDIR /app/src/

FROM registry.gitlab.com/wir-vs-climatecrisis/engelsystem/server:latest as development

COPY / /

COPY --from=node_builder /app/src/public/assets/ /app/src/public/assets
COPY --from=composer_builder /app/src/vendor/ /app/src/vendor/

RUN  cp /app/src/config/config.default.php /app/src/config/config.php

WORKDIR /app/src/

FROM registry.gitlab.com/wir-vs-climatecrisis/engelsystem/server:latest as ci

COPY / /

COPY --from=node_builder /app/src/public/assets/ /app/src/public/assets
COPY --from=composer_builder /app/src/vendor/ /app/src/vendor/

RUN  cp /app/src/config/config.default.php /app/src/config/config.php

WORKDIR /app/src/