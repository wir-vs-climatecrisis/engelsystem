<?php

use Engelsystem\ShiftCalendarRenderer;
use Engelsystem\ShiftsFilterRenderer;

/**
 *
 * @param array                 $location
 * @param ShiftsFilterRenderer  $shiftsFilterRenderer
 * @param ShiftCalendarRenderer $shiftCalendarRenderer
 * @return string
 */
function Location_view($location, ShiftsFilterRenderer $shiftsFilterRenderer, ShiftCalendarRenderer $shiftCalendarRenderer)
{
    $user = auth()->user();

    $assignNotice = '';
    if (config('signup_requires_arrival') && !$user->state->arrived) {
        $assignNotice = info(render_user_arrived_hint(), true);
    }

    $description = '';
    if (!empty($location['description'])) {
        $description = '<h3>' . __('Description') . '</h3>';
        $parsedown = new Parsedown();
        $description .= '<div class="well">' . $parsedown->parse($location['description']) . '</div>';
    }

    $tabs = [];
    if (!empty($location['map_url'])) {
        $tabs[__('Map')] = $location['map_url'];
    }

    $tabs[__('Shifts')] = div('first', [
        $shiftsFilterRenderer->render(page_link_to('locations', [
            'action'  => 'view',
            'location_id' => $location['RID']
        ])),
        $shiftCalendarRenderer->render()
    ]);

    $selected_tab = 0;
    $request = request();
    if ($request->has('shifts_filter_day')) {
        $selected_tab = count($tabs) - 1;
    }

    return page_with_title(glyph('map-marker') . $location['Name'], [
        $assignNotice,
        $description,
        tabs($tabs, $selected_tab)
    ]);
}

/**
 *
 * @param array $location
 * @return string
 */
function Location_name_render($location)
{
    if (auth()->can('view_locations')) {
        return '<a href="' . location_link($location) . '">' . glyph('map-marker') . $location['Name'] . '</a>';
    }

    return glyph('map-marker') . $location['Name'];
}
