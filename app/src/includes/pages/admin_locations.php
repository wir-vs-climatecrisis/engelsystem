<?php
/**
 * @return string
 */
function admin_locations_title()
{
    return __('Locations');
}

/**
 * @return string
 */
function admin_locations()
{
    $locations_source = Locations();
    $locations = [];
    $request = request();

    foreach ($locations_source as $location) {
        $locations[] = [
            'name'      => Location_name_render($location),
            'map_url'   => glyph_bool(!empty($location['map_url'])),
            'actions'   => table_buttons([
                button(
                    page_link_to('admin_locations', ['show' => 'edit', 'id' => $location['RID']]),
                    __('edit'),
                    'btn-xs'
                ),
                button(
                    page_link_to('admin_locations', ['show' => 'delete', 'id' => $location['RID']]),
                    __('delete'),
                    'btn-xs'
                )
            ])
        ];
    }

    $location = null;
    if ($request->has('show')) {
        $msg = '';
        $name = '';
        $url = null;
        $description = null;
        $location_id = 0;
        $lat = '';
        $lng = '';

        $angeltypes_source = AngelTypes();
        $angeltypes = [];
        $angeltypes_count = [];
        foreach ($angeltypes_source as $angeltype) {
            $angeltypes[$angeltype['id']] = $angeltype['name'];
            $angeltypes_count[$angeltype['id']] = 0;
        }

        if (test_request_int('id')) {
            $location = Location($request->input('id'));
            if (empty($location)) {
                redirect(page_link_to('admin_locations'));
            }

            $location_id = $request->input('id');
            $name = $location['Name'];
            $url = $location['map_url'];
            $description = $location['description'];
            $lat = $location['lat'];
            $lng = $location['lng'];

            $needed_angeltypes = NeededAngelTypes_by_location($location_id);
            foreach ($needed_angeltypes as $needed_angeltype) {
                $angeltypes_count[$needed_angeltype['angel_type_id']] = $needed_angeltype['count'];
            }
        }

        if ($request->input('show') == 'edit') {
            if ($request->hasPostData('submit')) {
                $valid = true;

                if ($request->has('name') && strlen(strip_request_item('name')) > 0) {
                    $result = Location_validate_name(strip_request_item('name'), $location_id);
                    if (!$result->isValid()) {
                        $valid = false;
                        $msg .= error(__('This name is already in use.'), true);
                    } else {
                        $name = $result->getValue();
                    }
                } else {
                    $valid = false;
                    $msg .= error(__('Please enter a name.'), true);
                }

                if ($request->has('map_url')) {
                    $url = strip_request_item('map_url');
                }

                if ($request->has('description')) {
                    $description = strip_request_item_nl('description');
                }

                foreach ($angeltypes as $angeltype_id => $angeltype) {
                    $angeltypes_count[$angeltype_id] = 0;
                    $queryKey = 'angeltype_count_' . $angeltype_id;
                    if (!$request->has($queryKey)) {
                        continue;
                    }

                    if (preg_match('/^\d{1,4}$/', $request->input($queryKey))) {
                        $angeltypes_count[$angeltype_id] = $request->input($queryKey);
                    } else {
                        $valid = false;
                        $msg .= error(sprintf(
                            __('Please enter needed angels for type %s.'),
                            $angeltype
                        ), true);
                    }
                }

                if ($valid) {
                    if (empty($location_id)) {
                        $location_id = Location_create($name, 0, $url, $description);
                    } else {
                        Location_update($location_id, $name, 0, $url, $description);
                    }

                    NeededAngelTypes_delete_by_location($location_id);
                    $needed_angeltype_info = [];
                    foreach ($angeltypes_count as $angeltype_id => $angeltype_count) {
                        $angeltype = AngelType($angeltype_id);
                        if (!empty($angeltype)) {
                            NeededAngelType_add(null, $angeltype_id, $location_id, $angeltype_count);
                            if ($angeltype_count > 0) {
                                $needed_angeltype_info[] = $angeltype['name'] . ': ' . $angeltype_count;
                            }
                        }
                    }

                    engelsystem_log(
                        'Set needed angeltypes of location ' . $name
                        . ' to: ' . join(', ', $needed_angeltype_info)
                    );
                    success(__('Location saved.'));
                    redirect(page_link_to('admin_locations'));
                }
            }
            $angeltypes_count_form = [];
            foreach ($angeltypes as $angeltype_id => $angeltype) {
                $angeltypes_count_form[] = div('col-lg-4 col-md-6 col-xs-6', [
                    form_spinner('angeltype_count_' . $angeltype_id, $angeltype, $angeltypes_count[$angeltype_id])
                ]);
            }

            return page_with_title(admin_locations_title(), [
                buttons([
                    button(page_link_to('admin_locations'), __('back'), 'back')
                ]),
                $msg,
                form([
                    div('row', [
                        div('col-md-6', [
                            form_text('name', __('Name'), $name, false, 35),
                            form_text('map_url', __('URL'), $url),
                            form_text('lat', __('Lat'), $lat),
                            form_text('lng', __('Lng'), $lng),

                            form_textarea('description', __('Description'), $description),
                            form_info('', __('Please use markdown for the description.')),
                        ]),
                        div('col-md-6', [
                            div('row', [
                                div('col-md-12', [
                                    form_info(__('Needed angels:'))
                                ]),
                                join($angeltypes_count_form)
                            ])
                        ])
                    ]),
                    form_submit('submit', __('Save'))
                ])
            ]);
        } elseif ($request->input('show') == 'delete') {
            if ($request->hasPostData('ack')) {
                Location_delete($location_id);

                engelsystem_log('Location deleted: ' . $name);
                success(sprintf(__('Location %s deleted.'), $name));
                redirect(page_link_to('admin_locations'));
            }

            return page_with_title(admin_locations_title(), [
                buttons([
                    button(page_link_to('admin_locations'), __('back'), 'back')
                ]),
                sprintf(__('Do you want to delete location %s?'), $name),
                form([
                    form_submit('ack', __('Delete'), 'delete btn-danger'),
                ], page_link_to('admin_locations', ['show' => 'delete', 'id' => $location_id])),
            ]);
        }
    }

    return page_with_title(admin_locations_title(), [
        buttons([
            button(page_link_to('admin_locations', ['show' => 'edit']), __('add'))
        ]),
        msg(),
        table([
            'name'      => __('Name'),
            'map_url'   => __('Map'),
            'lat' =>__('lat'),
            'lng' =>__('lng'),
            'actions'   => ''
        ], $locations)
    ]);
}
