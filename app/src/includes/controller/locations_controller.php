<?php

use Engelsystem\ShiftsFilter;
use Engelsystem\ShiftsFilterRenderer;

/**
 * Location controllers for managing everything Location related.
 */

/**
 * View a Location with its shifts.
 *
 * @return array
 */
function Location_controller()
{
    if (!auth()->can('view_locations')) {
        redirect(page_link_to());
    }

    $request = request();
    $Location = load_Location();

    $all_shifts = Shifts_by_location($Location);
    $days = [];
    foreach ($all_shifts as $shift) {
        $day = date('Y-m-d', $shift['start']);
        if (!in_array($day, $days)) {
            $days[] = $day;
        }
    }

    $shiftsFilter = new ShiftsFilter(
        true,
        [$Location['RID']],
        AngelType_ids()
    );
    $selected_day = date('Y-m-d');
    if (!empty($days)) {
        $selected_day = $days[0];
    }
    if ($request->has('shifts_filter_day')) {
        $selected_day = $request->input('shifts_filter_day');
    }
    $shiftsFilter->setStartTime(parse_date('Y-m-d H:i', $selected_day . ' 00:00'));
    $shiftsFilter->setEndTime(parse_date('Y-m-d H:i', $selected_day . ' 23:59'));

    $shiftsFilterRenderer = new ShiftsFilterRenderer($shiftsFilter);
    $shiftsFilterRenderer->enableDaySelection($days);

    $shiftCalendarRenderer = shiftCalendarRendererByShiftFilter($shiftsFilter);

    return [
        $Location['Name'],
        Location_view($Location, $shiftsFilterRenderer, $shiftCalendarRenderer)
    ];
}

/**
 * Dispatch different Location actions.
 *
 * @return array
 */
function Locations_controller()
{
    $request = request();
    $action = $request->input('action');
    if (!$request->has('action')) {
        $action = 'list';
    }

    switch ($action) {
        case 'view':
            return Location_controller();
        case 'list':
        default:
            redirect(page_link_to('admin_Locations'));
    }
}

/**
 * @param array $Location
 * @return string
 */
function Location_link($Location)
{
    return page_link_to('locations', ['action' => 'view', 'Location_id' => $Location['RID']]);
}

/**
 * @param array $Location
 * @return string
 */
function Location_edit_link($Location)
{
    return page_link_to('admin_Locations', ['show' => 'edit', 'id' => $Location['RID']]);
}

/**
 * Loads Location by request param Location_id
 *
 * @return array
 */
function load_Location()
{
    if (!test_request_int('Location_id')) {
        redirect(page_link_to());
    }

    $Location = Location(request()->input('Location_id'));
    if (empty($Location)) {
        redirect(page_link_to());
    }

    return $Location;
}
