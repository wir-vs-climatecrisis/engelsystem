<?php

use Engelsystem\Database\DB;
use Engelsystem\ValidationResult;

/**
 * Validate a name for a location.
 *
 * @param string $name    The new name
 * @param int    $locationId The Location id
 * @return ValidationResult
 */
function Location_validate_name($name, $locationId)
{
    $valid = true;
    if (empty($name)) {
        $valid = false;
    }

    if (count(DB::select('SELECT RID FROM `Location` WHERE `Name`=? AND NOT `RID`=?', [
            $name,
            $locationId
        ])) > 0) {
        $valid = false;
    }
    return new ValidationResult($valid, $name);
}

/**
 * returns a list of locations.
 *
 * @return array
 */
function Locations()
{
    return DB::select('SELECT * FROM `Location` ORDER BY `Name`');
}

/**
 * Returns Location id array
 *
 * @return array
 */
function Location_ids()
{
    $result = DB::select('SELECT `RID` FROM `Location`');
    return select_array($result, 'RID', 'RID');
}

/**
 * Delete a Location
 *
 * @param int $locationId
 */
function Location_delete($locationId)
{
    $Location = Location($locationId);
    DB::delete('DELETE FROM `Location` WHERE `RID` = ?', [
        $locationId
    ]);
    engelsystem_log('Location deleted: ' . $Location['Name']);
}

/**
 * Delete a Location by its name
 *
 * @param string $name
 */
function Location_delete_by_name($name)
{
    DB::delete('DELETE FROM `Location` WHERE `Name` = ?', [
        $name
    ]);
    engelsystem_log('Location deleted: ' . $name);
}

/**
 * Create a new Location
 *
 * @param string $name Name of the Location
 * @param boolean $from_frab Is this a frab imported Location?
 * @param string $url URL to a map tha can be displayed in an iframe
 * @param $description
 * @param string $lat
 * @param string $lng
 *
 * @return false|int
 */
function Location_create($name, $from_frab, $url, $description, $lat = 0, $lng = 0)
{
    DB::insert('
          INSERT INTO `Location` (`Name`, `from_frab`, `map_url`, `description`,`lat`, `lng`)
           VALUES (?, ?, ?, ?, ?, ?)
        ', [
        $name,
        (int)$from_frab,
        $url,
        $description,
        $lat,
        $lng
    ]);
    $result = DB::getPdo()->lastInsertId();

    engelsystem_log(
        'Location created: ' . $name
        . ', frab import: ' . ($from_frab ? 'Yes' : '')
        . ', map_url: ' . $url
        . ', description: ' . $description
        . ', lat: ' . $lat
        . ', lng: ' . $lng

    );

    return $result;
}

/**
 * update a Location
 *
 * @param int $locationId The Locations id
 * @param string $name Name of the Location
 * @param boolean $from_frab Is this a frab imported Location?
 * @param string $url URL to a map tha can be displayed in an iframe
 * @param string $description Markdown description
 * @param string $lat
 * @param string $lng
 *
 * @return int
 */
function Location_update($locationId, $name, $from_frab, $url, $description, $lat = '', $lng = '')
{
    $result = DB::update('
        UPDATE `Location`
        SET
            `Name`=?,
            `from_frab`=?,
            `lat`=?,
            `lng`=?,
            `map_url`=?,
            `description`=?
        WHERE `RID`=?
        LIMIT 1', [
        $name,
        (int)$from_frab,
        $lat,
        $lng,
        $url,
        $description,
        $locationId
    ]);

    engelsystem_log(
        'Location updated: ' . $name .
        ', frab import: ' . ($from_frab ? 'Yes' : '') .
        ', map_url: ' . $url .
        ', description: ' . $description
    );

    return $result;
}

/**
 * Returns Location by id.
 *
 * @param int $locationId RID
 * @return array|null
 */
function Location($locationId)
{
    $Location = DB::selectOne('
        SELECT *
        FROM `Location`
        WHERE `RID` = ?', [
        $locationId
    ]);

    return empty($Location) ? null : $Location;
}
