<?php

namespace Engelsystem\Renderer;

use Twig\Error\LoaderError;
use Twig\Loader\FilesystemLoader;

class TwigLoader extends FilesystemLoader
{
    /**
     * @param string $name
     * @param bool   $throw
     * @return false|string
     * @throws LoaderError
     */
    public function findTemplate($name, $throw = true)
    {
        $extension = '.twig';
        $extensionLength = strlen($extension);
        if (substr($name, -$extensionLength, $extensionLength) !== $extension) {
            $name .= $extension;
        }

        return parent::findTemplate($name, $throw);
    }
}
