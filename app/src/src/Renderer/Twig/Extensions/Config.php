<?php

namespace Engelsystem\Renderer\Twig\Extensions;

use Engelsystem\Config\Config as EngelsystemConfig;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class Config extends AbstractExtension
{
    /** @var EngelsystemConfig */
    protected $config;

    /**
     * @param EngelsystemConfig $config
     */
    public function __construct(EngelsystemConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('config', [$this->config, 'get']),
        ];
    }
}
