<?php

namespace Engelsystem\Renderer\Twig\Extensions;

use Engelsystem\Helpers\Translator;
use Symfony\Bridge\Twig\TokenParser\TransTokenParser;
use Twig\Extension\AbstractExtension;
use Twig\TokenParser\TokenParserInterface;
use Twig\TwigFilter;
use Twig\TwigFunction;

class Translation extends AbstractExtension
{
    /** @var Translator */
    protected $translator;

    /** @var TokenParserInterface */
    protected $tokenParser;

    /**
     * @param Translator             $translator
     * @param TokenParserInterface   $tokenParser
     */
    public function __construct(Translator $translator, TransTokenParser $tokenParser)
    {
        $this->translator = $translator;
        $this->tokenParser = $tokenParser;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new TwigFilter('trans', [$this->translator, 'translate']),
        ];
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('__', [$this->translator, 'translate']),
            new TwigFunction('_e', [$this->translator, 'translatePlural']),
        ];
    }

    /**
     * @return TokenParserInterface[]
     */
    public function getTokenParsers()
    {
        return [$this->tokenParser];
    }
}
