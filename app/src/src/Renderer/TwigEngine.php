<?php

namespace Engelsystem\Renderer;

use Twig\Environment;
use Twig\Error\SyntaxError;

class TwigEngine implements EngineInterface
{
    /** @var Environment */
    protected $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * Render a twig template
     *
     * @param string $path
     * @param array $data
     *
     * @return string
     * @throws SyntaxError
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     */
    public function get($path, $data = [])
    {
        return $this->twig->render($path, $data);
    }

    /**
     * @param string $path
     * @return bool
     */
    public function canRender($path)
    {
        return $this->twig->getLoader()->exists($path);
    }
}
