<?php

namespace Engelsystem\Http;

use Zend\Diactoros\ServerRequestFactory as DiactorosServerRequestFactoryAlias;

/**
 * @author Maximilian Berghoff <Maximilian.Berghoff@mayflower.de>
 */
class ServerRequestFactory extends DiactorosServerRequestFactoryAlias
{
}
