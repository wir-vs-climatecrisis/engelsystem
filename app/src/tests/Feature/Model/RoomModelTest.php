<?php

namespace Engelsystem\Test\Feature\Model;

use PHPUnit\Framework\TestCase;

class LocationModelTest extends TestCase
{
    private $location_id = null;

    public function createLocation()
    {
        $this->location_id = Location_create('test', false, null, null);
    }

    public function testLocation()
    {
        $this->createLocation();

        $location = Location($this->location_id);

        $this->assertNotEmpty($location);
        $this->assertNotNull($location);
        $this->assertEquals($location['Name'], 'test');

        $this->assertEmpty(Location(-1));
    }

    public function tearDown()
    {
        if ($this->location_id != null) {
            Location_delete($this->location_id);
        }
    }
}
