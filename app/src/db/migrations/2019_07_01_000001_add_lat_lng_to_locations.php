<?php

namespace Engelsystem\Migrations;

use Engelsystem\Database\Migration\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddLatLngToLocations extends Migration
{
    /**
     * Run the migration
     */
    public function up()
    {
        if (!$this->schema->hasTable('Location')) {
            return;
        }

        $connection = $this->schema->getConnection();
        if (!$connection->getSchemaBuilder()->hasColumn('Location', 'lat')) {
            $this->schema->table('Location', function (Blueprint $table) {
                $table->addColumn('string', 'lat',['length' => 50]);
            });
        }
        $connection = $this->schema->getConnection();
        if (!$connection->getSchemaBuilder()->hasColumn('Location', 'lng')) {
            $this->schema->table('Location', function (Blueprint $table) {
                $table->addColumn('string', 'lng',['length' => 50]);
            });
        }
    }

    /**
     * Down is not possible and not needed since this is a bugfix.
     */
    public function down()
    {
    }
}
